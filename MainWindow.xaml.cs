﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows;
using System.Windows.Controls;
using System.Windows.Data;
using System.Windows.Documents;
using System.Windows.Input;
using System.Windows.Media;
using System.Windows.Media.Animation;
using System.Windows.Media.Imaging;
using System.Windows.Navigation;
using System.Windows.Shapes;

namespace AnimationWPF
{
    /// <summary>
    /// Логика взаимодействия для MainWindow.xaml
    /// </summary>
    public partial class MainWindow : Window
    {
        public MainWindow()
        {
            InitializeComponent();


                DoubleAnimation animation = new DoubleAnimation();
                animation.From = rectangle.Width;
                animation.To = rectangle.Width + 250;
                animation.Duration = new Duration(TimeSpan.FromSeconds(5));
                //animation.Completed += StoryboardCompleted;

                Storyboard storyboard = new Storyboard();
                storyboard.Children.Add(animation);
                Storyboard.SetTargetName(animation, rectangle.Name);
                Storyboard.SetTargetProperty(animation, new PropertyPath(WidthProperty));
                storyboard.Begin(this);


                DoubleAnimation anime = new DoubleAnimation();
                anime.From = rectangle02.Width;
                anime.To = rectangle02.Width + 250;
                anime.Duration = new Duration(TimeSpan.FromSeconds(7));
                anime.Completed += StoryboardCompleted;

                Storyboard board = new Storyboard();
                board.Children.Add(anime);
                Storyboard.SetTargetName(anime, rectangle02.Name);
                Storyboard.SetTargetProperty(anime, new PropertyPath(WidthProperty));
                board.Begin(this);


                DoubleAnimation animi = new DoubleAnimation();
                animi.From = rectangle03.Width;
                animi.To = rectangle03.Width + 250;
                animi.Duration = new Duration(TimeSpan.FromSeconds(3));
               // anime.Completed += StoryboardCompleted;

                Storyboard boar = new Storyboard();
                boar.Children.Add(animi);
                Storyboard.SetTargetName(animi, rectangle03.Name);
                Storyboard.SetTargetProperty(animi, new PropertyPath(WidthProperty));
                boar.Begin(this);
            

        }

        private void StoryboardCompleted(object sender, EventArgs e)
        {
                text.Content = "Готово!";
        }
        
    }
}
